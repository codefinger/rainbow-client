module main

go 1.13

require (
	github.com/glycerine/rbtree v0.0.0-20190406191118-ceb71889d809
	github.com/pkg/errors v0.9.1
	github.com/satori/go.uuid v1.2.0
	gopkg.in/yaml.v2 v2.2.8
)

# 内网穿透原理图
![Image text](./rainbow-diagram.jpg)

- 首先必须要有一台具有独立外网IP的服务器
- 分别在服务器中安装：rainbow-server，在提供服务的内网目标主机上安装：rainbow-client
- 两端配置并启动后，就可实现内网穿透
- 端口充足的情况下，一个服务端可同时支持任意多个内网主机对外提供服务

# 构建
`先cd到源码目录`
## Windows
`go mod download`

`go build -o rainbow-client.exe`

## Linux
`go mod download`

`go build -o rainbow-client`

## 注意
如果 `go mod download` 下载依赖包失败，请自行网上查找代理或镜像

# 配置
```
server-address: 127.0.0.1:9900 #服务器地址（必须，rainbow-server的服务地址）
host-name: mi-pc #主机名称（必须，与rainbow-server中的主机名对应）
secret-key: B0NuUj9UJ7dcnJm3UivZZgO7nrln1cQ8bK06LEcG4LxAKR #主机密钥（必须，与rainbow-server中的主机密钥对应）
host-read-timeout: 3m #主机读超时
host-write-timeout: 3m #主机写超时
target-read-timeout: 3m #目标读超时
target-write-timeout: 3m #目标写超时
sign-timeout: 30s #签名有效期
reconnect-interval: 5s #断线重连间隔时长
keep-alive-interval: 1m #心跳间隔时长
max-packet-body-size: 65520 #最大包结构体大小（只针对私有协议）
transport-buffer-size: 65536 #传输缓冲区大小
```

# 启动
## Windows
`rainbow-client.exe -c rainbow-client.yml`

## Linux
`./rainbow-client -c rainbow-client.yml`

# 服务端
[https://gitee.com/codefinger/rainbow-server](https://gitee.com/codefinger/rainbow-server)
